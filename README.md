
# Data Quality LCA #

## Description
This is an LCA that makes common changes to an attribute's values within an entity. This LCA uses a properties file to decide which Attribute and how it should be manipulated. Please use this LCA as an example in creating your own LCA.



##Change Log

```
#!plaintext

Last Update Date: 06/22/2017
Version: 1.0.0
Description: Initial version
```

##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/lca-dataquality/src/93b9df5ca0b8dd16fe8425de2dbad34aaad596db/data-quality.lca/QuickStart.md?at=master&fileviewer=file-view-default).


