# Quick Start 

##Building
Package the LCA by navigating to the folder the LCA is in and running mvn package or via Intellij by selecting Package in the Maven sidebar

##Dependencies 

1. aws-java-sdk-bom 1.11.132
2. aws-java-sdk-s3 1.11.132
3. life-cycle-framework-lambda (2017.1.0.0.0,2017.1.1.0.0]
4. reltio-life-cycle-test	(2017.1.0.0.0,2017.1.1.0.0]
5. junit 4.12
6. jsonassert 1.3.0


##Parameters File Example

```
# Properties definining the LCA
ERROR=Ignore this property, for some reason I can't get the code to read the first property
BLANK_ATTRIBUTE=FirstName
DEFAULT_VALUE=No Name Provided
LENGTH_ATTRIBUTE=TaxID
LENGTH=9
RANGE_ATTRIBUTE=NoB
MAX=5
MIN=1
CAPITALIZE_ATTRIBUTE=Description
DATE_ATTRIBUTE=DoB
#Has to be Year-Month-Day format Ex: 2017-03-26
DATE=2017-5-15

```
##Executing
Please review the development spec for Deployment